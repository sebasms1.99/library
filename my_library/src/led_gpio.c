/**
 * *******************************************************************************
 * @file    led_gpio.c
 * @author  Sebastian Mosquera Salazar
 * @brief   animation LEDs
 * @date    19-05-2023
 * @version v.1.0
 * *******************************************************************************
*/

/* Includes -----------------------------------------------------------------------------------------------------------------------*/
#include"led_gpio.h"

/* Private defines ----------------------------------------------------------------------------------------------------------------*/
/* Private typedef  ---------------------------------------------------------------------------------------------------------------*/
/* Private macro ------------------------------------------------------------------------------------------------------------------*/
/* Private Variables declaration --------------------------------------------------------------------------------------------------*/
GPIO_TypeDef* g_LED_PORT;
uint16_t g_LED_PIN;

/* Private function prototypes ----------------------------------------------------------------------------------------------------*/
/* Functions definition -----------------------------------------------------------------------------------------------------------*/

/**
 * @brief this function configures the GPIOs, chooses the port and the pin you want to configure it as output for the LED.
 * 
 * @param port : port you want
 * @param pin : pin you want
 */

void LED_GPIO_Setting_Init(GPIO_TypeDef* port, uint16_t pin)
{
    g_LED_PORT = port;
    g_LED_PIN = pin;

    GPIO_InitTypeDef GPIO_InitStruct = {0};
    /* GPIO Ports Clock Enable */
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOG_CLK_ENABLE();

    /*Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(g_LED_PORT, g_LED_PIN, GPIO_PIN_RESET);

    /*Configure GPIO pins */
    GPIO_InitStruct.Pin = g_LED_PIN;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(g_LED_PORT, &GPIO_InitStruct);
    return 0;
}

/**
 * @brief Turn on the LED
 * 
 */
void LED_GPIO_On(void)
{
    HAL_GPIO_WritePin(g_LED_PORT, g_LED_PIN, GPIO_PIN_SET);
}

/**
 * @brief Turn off the LED
 * 
 */
void LED_GPIO_Off(void)
{
    HAL_GPIO_WritePin(g_LED_PORT, g_LED_PIN, GPIO_PIN_RESET);
}

/**
 * @brief Animation is defined
 * 
 */
void LED_GPIO_Animation_Start(void)
{
    /*Static LED for 2 seconds */
    LED_GPIO_On(); // LED on
    HAL_Delay(2000); 

    /*Blink for 5 seconds */
    for (int i = 0; i < 10; i++)
    {
        LED_GPIO_On(); // LED on
        HAL_Delay(250); 

        LED_GPIO_Off(); // LED off
        HAL_Delay(250); 
    }

    /*Static LED for 2 seconds */
    LED_GPIO_On(); // LED on
    HAL_Delay(2000); 

    /*LED turns on and off 5 times */
    for (int i = 0; i < 5; i++)
    {
        LED_GPIO_On(); // LED on
        HAL_Delay(500); 

        LED_GPIO_Off(); // LED off
        HAL_Delay(500); 
    }
    /*LED off for 2 seconds */
    LED_GPIO_Off(); // LED off
    HAL_Delay(2000); 

    /*Blink 2 times*/
    for (int i = 0; i < 4; i++)
    {
        LED_GPIO_On(); // LED on
        HAL_Delay(250); 

        LED_GPIO_Off(); // LED off
        HAL_Delay(250); 
    }
    return 0;
}
/*-------------------------------------------------------- END OF FILE -------------------------------------------------------------*/
