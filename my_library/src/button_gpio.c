/**
 * *******************************************************************************
 * @file    led_gpio.c
 * @author  Sebastian Mosquera Salazar
 * @brief   animation LEDs
 * @date    19-05-2023
 * @version v.1.0
 * *******************************************************************************
*/

/* Includes -----------------------------------------------------------------------------------------------------------------------*/
#include"button_gpio.h"

/* Private defines ----------------------------------------------------------------------------------------------------------------*/
/* Private typedef  ---------------------------------------------------------------------------------------------------------------*/
/* Private macro ------------------------------------------------------------------------------------------------------------------*/
/* Private Variables declaration --------------------------------------------------------------------------------------------------*/
GPIO_TypeDef* g_BUTTON_PORT; // Puerto GPIO donde está conectado el botón de usuario
uint16_t g_BUTTON_PIN;

/* Private function prototypes ----------------------------------------------------------------------------------------------------*/
/* Functions definition -----------------------------------------------------------------------------------------------------------*/

/**
 * @brief This function configures the GPIOs, choose the port and the pin that you want to configure as input for the Button.
 * 
 * @param port 
 * @param pin 
 */
void BUTTON_GPIO_Setting_Init(GPIO_TypeDef* port, uint16_t pin)
{
    g_BUTTON_PORT = port;
    g_BUTTON_PIN = pin;

    GPIO_InitTypeDef GPIO_InitStruct = {0};
    /* GPIO Ports Clock Enable */
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOG_CLK_ENABLE();

    /*Configure GPIO pin */
    //GPIO_InitTypeDef GPIO_InitStruct;
    GPIO_InitStruct.Pin = g_BUTTON_PIN;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(g_BUTTON_PORT, &GPIO_InitStruct);
}

/**
 * @brief Read button state
 * 
 * @return GPIO_PinState 
 */
GPIO_PinState BUTTON_GPIO_GetState(void)
{
    return HAL_GPIO_ReadPin(g_BUTTON_PORT, g_BUTTON_PIN);
}
/*-------------------------------------------------------- END OF FILE -------------------------------------------------------------*/
